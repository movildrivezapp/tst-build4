FROM nginx:alpine

RUN apk add --update --no-cache python3 python3-dev bash nano gcc musl-dev g++ gpgme-dev curl 
RUN apk add --no-cache libc-dev make git libffi-dev openssl-dev libxml2-dev libxslt-dev build-base
RUN apk add --no-cache --virtual .build-deps linux-headers
RUN apk add --update nodejs
RUN apk add --update npm
RUN npm install -g firebase-tools

######################
# Install Python3 
######################
ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3 py3-pip && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools
RUN pip3 install --no-cache Flask==1.1.2 flask_cors flask-restful flask_restful python-dotenv pymongo[srv] dnspython simplejson requests firebase firebase-admin

#RUN apk add --no-cache python3 && \
#python3 -m ensurepip && \
#pip3 install --upgrade pip setuptools \
#&& rm -r /usr/lib/python*/ensurepip && \
#if [ ! -e /usr/bin/pip ]; then (ln -s pip3 /usr/bin/pip) && echo "********* pip en /usr/bin/pip ******" ; fi && \
#if [[ ! -e /usr/bin/python ]]; then (ln -sf /usr/bin/python3 /usr/bin/python) && echo "********* python en /usr/bin/python3 ******" ; fi && \
#rm -r /root/.cache

#RUN pip3 --no-cache-dir install -r requirements.txt

#WORKDIR /app
#COPY . /app

EXPOSE 9091

ENTRYPOINT ["/usr/bin/python3"]
#  ENTRYPOINT ["bash", "entrypoint.sh"]
CMD ["app.py"]
