#!/bin/bash/python3
from flask import Flask, request, redirect, jsonify, abort, Response
from flask_restful import Resource, Api
from flask_cors import CORS
from flask import request
from os.path import isfile, join
from os import listdir
from bson.json_util import dumps
#import connect_db
import sys
import os
import simplejson
import json
import requests
import collections
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

app = Flask(__name__)
CORS(app)
api = Api(app)
port = 9091

@app.route('/status', methods=['GET'])
def get_status():
    return "Carpooling Service OK !"

@app.route('/<cars>', methods=['GET', 'PUT'])
def handle_cars(cars):

    # Data received in the PUT
    data=request.get_json(force=True)

    ########## GET Cars from database #######
    if request.method=='GET':
        print("GET will display existing cars by quering MongoDB/Firebase")
        return "GET on /cars"

    ########## PUT Cars to database #######
    else:
        print('---> PUT cars received!!!')
        print('---> JSON received is:', data)
        print('-------------------------------')
        print('---> Loading cars to Firebase..')
        print('-------------------------------')
        url_firebase='https://carpoolingchallenge-default-rtdb.europe-west1.firebasedatabase.app/'
        url_cars=url_firebase+cars+'.json'
        url_assigned_journeys=url_firebase+'assigned_journeys.json'
        r1=requests.put(url_cars,data=json.dumps(data, indent=4))
        print('---> result PUT /cars: ', r1.status_code)

        if r1.status_code != 200:
            print('Not ok...')
            return r1.status_code

        else:
            # Creates an skeleton body list to trigger API requests
            # d={'0':{"car_id": 0, "group_id": 0, "people": 0, "seats": 0, "pool": 0 , "ps": 0}}
            # r2=requests.put(url_assigned_journeys,data=json.dumps(d, indent=4))
            # print('---> result PUT /assigned_journeys: ', r2.status_code)
            print('-------------------------------')
            print('---> Creating pooling service!!')
            print('-------------------------------')
            # if r2.status_code != 200:
            #     return r2.status_code

    return 'OK'

@app.route('/<journey>', methods=['POST'])
def handle_journey(journey):
    data=request.get_json(force=True)

    ########## GET Cars from database #######
    if request.method=='GET':
        print("GET will display existing journeys by quering MongoDB/Firebase")
        return "GET on /journey"

    else:
        print('---> POST journey received!!!')
        print('----------------------------------')
        print('---> Loading journey to Firebase..')
        print('----------------------------------')
        url_journeys='https://carpoolingchallenge-default-rtdb.europe-west1.firebasedatabase.app/journeys.json'
        r=requests.put(url_journeys,data=json.dumps(data, indent=4))
        print('---> resultado del POST request: ', r.status_code)

        if r.status_code != 200:
            print('Not ok...')
            return False

        else:
            check_availability_in_cars(data)

    return "OK"

def check_availability_in_cars(journey):

    # Get journey_id for App instantiation
    journey_name='journey_'+str(journey['id'])
    print("---> journey is: ", str(journey))
    print("---> journey_name: ", journey_name)
    print("---> people: ", journey['people'])

    # Get Firebase Credentials to access database
    cred = credentials.Certificate('./carpoolingchallenge-firebase-adminsdk-3wj0m-55c71fbdb6.json')
    url_firebase = 'https://carpoolingchallenge-default-rtdb.europe-west1.firebasedatabase.app/'

    # Initialize the app to instantiate the journey granting admin privileges
    AppJourney=initialize_app(cred, journey_name)

    print("---> check_availability first in /assigned_journeys ", str(journey))
    print('---> searching a car with ', str(journey['people']),' seats available in running /assigned_journeys')
    people=str(journey['people'])
    group_id=str(journey['id'])

    # Define the db references for querying /assigned_journeys started and /cars available
    cars_ref = db.reference('/cars')
    cars = cars_ref.get()
    assigned_journeys_ref = db.reference('/assigned_journeys')
    assigned_journeys = assigned_journeys_ref.get()

    # Define counters to iterate by /assigned_journeys started and /cars list
    j=0

    # No pool found at the beginning
    found=0

    # PS pooling service by default is 0
    ps=0

    # No pool found on current /assigned_journeys
    print('************* ')
    print('************* found 1', found)
    print('************* ')

    # First search for pools available in /assigned_journeys
    found=check_first_in_assigned_journeys(people, group_id, assigned_journeys)

    # Found pool? after searching in /assigned_journeys
    print('************* ')
    print('************* found 2', found)
    print('************* ')

    # Second search for pools available in /cars if not found
    if (found != 1):

        print('************* ')
        print('************* found 3', found)
        print('************* ')

        for car in cars:

            if (int(cars[j]['seats'])) >= int(journey['people']):

                seats=cars[j]['seats']
                car_id=cars[j]['id']
                print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
                print('************* found pool available in /cars: ', car_id)
                print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')

                # Define firebase url
                url_assigned_journeys=url_firebase+'assigned_journeys.json'
                url_cars=url_firebase+'cars.json'
                url_journeys=url_firebase+'journeys.json'

                # Define the pooling free seats
                pool=int(seats)-int(people)

                # By default car is not pooling (ps = 0) and not first pooling (fps = 0)
                ps=0

                # Define data journey
                data_journey={ "car_id": int(car_id), "group_id": int(group_id), "people": int(people), "seats": int(seats), "pool": int(pool) , "ps": int(ps) }
                print(data_journey)

                # PUT a new journey in /assigned_journeys
                url_assigned_journey=url_firebase+'assigned_journeys/'+str(group_id)+'.json'
                r=requests.put(url_assigned_journey,data=json.dumps(data_journey, indent=4))
                print('---> journey succesfully started! ', r.status_code)

                if r.status_code != 200:
                    print('Not ok...')

                else:
                    print('-------------------------------')
                    print('---> Everythings ok............')
                    print('-------------------------------')

                print('---> make car unavailable!!!!!')

                # Update /cars list
                cars2=cars.pop(j)
                print('-------------------------------------------------------------------')
                print(cars2)
                print('------------ print /cars without the index of the journey ---------')
                print(cars)
                print('-------------------------------------------------------------------')

                r=requests.put(url_cars,data=json.dumps(cars, indent=4))
                if r.status_code != 200:
                    print('Not ok...')

                else:
                    print('--------------------------------------')
                    print('---> Car assigned, remove the journey.')
                    print('--------------------------------------')
                    r=requests.delete(url_journeys)

                    if (r.status_code != 200):
                        print('Not ok...')

                    else:
                        print('-------------------------------')
                        print('---> Everythings ok............')
                        print('-------------------------------')

                # Delete the app instatiated for the journey
                delete_app(AppJourney)
                break

            else:
                j+=1
                print('************* Pool was not found in index: ', j)

    else:
        print('************* Pool was already found in /assigned_journeys')
        # Delete the app instatiated for the journey
        delete_app(AppJourney)
        return '', 200

def check_first_in_assigned_journeys(new_people, new_group_id, assigned_journeys):

    print('************* new_people', new_people)
    print('************* new_group_id', new_group_id)
    print('************* assigned_journeys', assigned_journeys )
    print('************* type(assigned_journeys)', type(assigned_journeys))

    # Check /assigned_journeys types
    if isinstance(assigned_journeys, dict):

        # Iterate through /assigned_journeys
        for k,v in assigned_journeys.items():

            print(' Journey: ', k, ', has a pool of: ', v['pool'], ' seats')
            print(' for the new group of', new_people, ' people.')

            # If there is pool available in car that is not doing pooling (ps=0)
            if (v['pool'] >= int(new_people)) and not v['ps']:
                print('///////////// found pool in:', k)
                print('************* con values: ', v)

                # Change ps=1 in car when found /assigned_journeys:
                new_ps=1
                found=1

                # Define elements of k
                car_id = v['car_id']
                group_id = v['group_id']
                people = v['people']
                pool = v['pool']
                seats = v['seats']

                # First PUT to enable pooling on the car (ps=1)
                data_journey={ "car_id": int(car_id), "group_id": int(group_id), "people": int(people), "seats": int(seats), "pool": int(pool), "ps": int(new_ps) }
                print('+++++ new_data for journey: +++++++', k)
                print(data_journey)
                print('+++++++++++++++++++++++++++++++++++++++++++', k)
                url_firebase='https://carpoolingchallenge-default-rtdb.europe-west1.firebasedatabase.app/'
                url=url_firebase+'assigned_journeys/'+str(group_id)+'.json'

                r1=requests.put(url,data=json.dumps(data_journey, indent=4))
                if r1.status_code != 200:
                    print('Not ok...')

                else:
                    print('---------------------------------------')
                    print('---> changed ps = 1 in k............', k)
                    print('---------------------------------------')

                # Second PUT insert new journey on /assigned_journeys
                new_pool=int(pool)-int(new_people)
                new_data_journey={ "car_id": int(car_id), "group_id": int(new_group_id), "people": int(new_people), "seats": int(seats), "pool": int(new_pool), "ps": int(new_ps) }
                print('+++++ new_data_journey k +++++++++++++++++++++++++++++', new_group_id)
                print(new_data_journey)
                print('+++++++++++  assigned_journeys +++++++++++++++++++++++')
                assigned_journeys_ref = db.reference('/assigned_journeys')
                assigned_journeys = assigned_journeys_ref.get()
                print(assigned_journeys)
                print('++++++++++++++++++++++++++++++++++++++++++++++++++++++')

                # Query again on /assigned_journeys to insert the new journey with updated pools
                assigned_journeys[str(new_group_id)]=new_data_journey
                print('+++++++++++  new_assigned_journeys +++++++++++++++++++')
                print(assigned_journeys)
                print('++++++++++++++++++++++++++++++++++++++++++++++++++++++')

                url_firebase='https://carpoolingchallenge-default-rtdb.europe-west1.firebasedatabase.app/'
                url2=url_firebase+'assigned_journeys/'+'.json'
                r2=requests.put(url2,data=json.dumps(assigned_journeys, indent=4))
                if r2.status_code != 200:
                    print('Not ok...')

                else:
                    print('--------------------------------------')
                    print('---> Pool assigned, remove the journey')
                    print('--------------------------------------')
                    url_journey=url_firebase+'/journeys.json'
                    r=requests.delete(url_journey)

                    if (r.status_code != 200):
                        print('Not ok...')

                    else:
                        # Delete the App instantiation
                        #delete_app(AppJourney)
                        print('--------------------------------------')
                        print('---> Everythings ok............', found)
                        print('--------------------------------------')
                        # Stop the iteration loop
                        break

            else:
                print('************* No pool available in Journey', k)
                found=0
                print('************* found', found)

    # First pooling service or pool not found in /assigned_journeys
    else:
        found=0

    return found

def initialize_app(credentials, name):
    App_=firebase_admin.initialize_app(credentials, {'databaseURL': 'https://carpoolingchallenge-default-rtdb.europe-west1.firebasedatabase.app/'})
    print("---> initializing app: ", App_)
    return App_

def delete_app(App_):
    firebase_admin.delete_app(App_)
    print("---> deleting app: ", App_.name)

@app.route('/dropoff', methods=['POST'])
def handle_dropoff():

    if request.content_type == 'application/json':
        data=request.get_json(force=True)
        print("---> data in JSON: ", data)
    elif request.content_type == 'application/x-www-form-urlencoded':
        data_form=request.get_data()
        print("---> data in x-www-form: ", data_form.decode("utf-8"))
    else:
        print("---> can't parse data from POST")

    ########## GET Car ID from assigned journeys #######
    print('---> POST dropoff received in !!!')
    print('----------------------------------')
    print('---> Loading dropoff to Firebase..')
    print('----------------------------------')
    url_firebase='https://carpoolingchallenge-default-rtdb.europe-west1.firebasedatabase.app/'
    dropoff=data_form.decode("utf-8")
    dropoff_arr=dropoff.split('=')
    dropoff_id=str(dropoff_arr[1])
    journey_name='journey_'+'dropoff_id'

    # Get Firebase Credentials to access database
    cred = credentials.Certificate('./carpoolingchallenge-firebase-adminsdk-3wj0m-55c71fbdb6.json')
    url_assigned_journeys = url_firebase+'assigned_journeys'
    url_journeys = url_firebase+'journeys'

    # Initialize the app to instantiate the dropoff granting admin privileges
    AppDropoff=initialize_app(cred, journey_name)

    # Get instances of the db for querying cars/ and /assigned_journeys
    assigned_journeys_ref = db.reference('/assigned_journeys')
    assigned_journeys = assigned_journeys_ref.get()
    cars_ref = db.reference('/cars')
    cars = cars_ref.get()
    dropoff=assigned_journeys[dropoff_arr[1]]
    car_id=dropoff['car_id']
    seats=dropoff['seats']
    ps=dropoff['ps']
    people=dropoff['people']
    group_id=dropoff['group_id']

    print('***************** car_id', car_id)
    print('***************** seats', seats)
    print('***************** ps', ps)
    print('***************** seats', people)
    print('***************** group_id', group_id)

    # If ps=1 car doing pooling and we return it to /assigned_journeys
    if ps:
        print('-------------------------------------')
        print('---> Bringing Pools Back to Car......')
        print('-------------------------------------')

        # Search the car_id of the dropoff in /assigned_journeys
        for k,v in assigned_journeys.items():
            if v['car_id'] == car_id:
                print('***************** found the car_id in /assigned_journeys', k)

                # Define elements of k
                car_id = v['car_id']
                new_group_id = v['group_id']
                people = v['people']
                pool = v['pool']
                seats = v['seats']

                # Enable again car-pooling in car
                new_ps=0

                # Increment the pools of the car with the people of the dropoff
                new_pool=pool+people

                # Firtst PUT (or patch) the set back ps=0 in the car
                data_journey={ "car_id": car_id, "group_id": new_group_id, "people": people, "pool": new_pool , "ps": new_ps, "seats": seats}
                print('+++++ new_pools for journey: +++++++++++++++++++++++++++++++++++++++++++++++')
                print(data_journey)
                print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
                url_firebase='https://carpoolingchallenge-default-rtdb.europe-west1.firebasedatabase.app/'
                url=url_firebase+'assigned_journeys/'+str(new_group_id)+'.json'

                r1=requests.put(url,data=json.dumps(data_journey, indent=4))
                if r1.status_code != 200:
                    print('Not ok...')

                else:
                    print('---------------------------------------')
                    print('---> Changed ps = 0 in k............', k)
                    print('---------------------------------------')
                    break

        # Second DELETE the journey of the dropoff
        print('+++++ deleting journey: ++++++++++++++')
        print('+++++++++++++++++++++++++++++++++++++++')
        url_firebase='https://carpoolingchallenge-default-rtdb.europe-west1.firebasedatabase.app/'
        url=url_firebase+'assigned_journeys/'+str(group_id)+'.json'

        r2=requests.delete(url)
        if r1.status_code != 200:
            print('Not ok...')

        else:
            print('---------------------------------------')
            print('---> Journey deleted and car has now ', new_pool, 'seats more available!')
            print('---------------------------------------')

            # Delete de App instantiated for the dropoff
            delete_app(AppDropoff)

    # If ps=0 the car is not doing Carpooling and car will be put back in /cars
    else:
        url_assigned_journey=url_assigned_journeys+'/'+dropoff_id+'.json'
        r=requests.delete(url_assigned_journey)
        if (r.status_code != 200):
            print('Not ok...')

        else:
            #Return Car to /cars
            print('-------------------------------------')
            print('---> Bringing Car Back to Service....')
            print('-------------------------------------')

            # Get Car ID from passed dropoff_id data
            car={"id": car_id, "seats": seats}
            print(car)
            print('-------------------------------------')

            # Insert car_id in the correspondent index
            #   Here a location strategy, or a preferred car can be implemented
            #       By default an available car will be inserted at the beginning of the list pos[0]
            index=0
            cars.insert(index,car)

            # Define /cars url
            url_cars='https://carpoolingchallenge-default-rtdb.europe-west1.firebasedatabase.app/cars/.json'

            # PUT back /cars available to database
            r=requests.put(url_cars,data=json.dumps(cars, indent=4))

            if r.status_code != 200:
                print('Not ok...')
                return False
            else:
                print('---> car succesfully put back on service! ', r.status_code)

                # Delete de App instantiated for the dropoff
                delete_app(AppDropoff)

    return '', 200

@app.route('/locate', methods=['POST'])
def handle_locate():
    if request.content_type == 'application/json':
        data=request.get_json(force=True)
        print("---> data in JSON: ", data)
    elif request.content_type == 'application/x-www-form-urlencoded':
        data_form=request.get_data()
        print("---> data in x-www-form: ", data_form.decode("utf-8"))
    else:
        print("---> can't parse data from POST")

    ########## GET Car ID from assigned journeys #######
    print('---> POST locate received in !!!')
    print('----------------------------------')
    print('---> Loading location in Firebase..')
    print('----------------------------------')
    url_firebase='https://carpoolingchallenge-default-rtdb.europe-west1.firebasedatabase.app/'
    locate=data_form.decode("utf-8")
    locate_arr=locate.split('=')
    locate_id=str(locate_arr[1])
    locate_id_index=int(locate_id)
    print('---> ID to search for: ', locate_id)
    journey_name='journey_'+'locate_id'
    print('---> locate_id in journey: ', locate_id_index)

    # Get Firebase Credentials to access database
    cred = credentials.Certificate('./carpoolingchallenge-firebase-adminsdk-3wj0m-55c71fbdb6.json')
    url_assigned_journeys = url_firebase+'assigned_journeys'

    # Initialize the app to instantiate the location
    AppLocate=initialize_app(cred, journey_name)

    # Get instances of the db for querying /assigned_journeys
    assigned_journeys_ref = db.reference('/assigned_journeys')
    assigned_journeys = assigned_journeys_ref.get()

    #Iterate through assigned_journeys
    for assigned_journey in assigned_journeys:
        try:
            if(assigned_journeys[''+locate_id+'']['car_id']):
                car_id=int(assigned_journeys[''+locate_id+'']['car_id'])
                seats=int(assigned_journeys[''+locate_id+'']['seats'])
                print('---> found group in car ID: ', car_id)
                car={"id": car_id, "seats": seats}
                delete_app(AppLocate)
                return car

        except Exception as e:
            return "Error 404. Not found"

    delete_app(AppLocate)

# Incoming data will be checked against this JSON Body Schemas
def check_incoming_json(JSON):
    schema_cars = {
        "type": "object",
        "properties": {
            "id": { "type": "number"},
            "seats": { "type": "number"}
        },
        "required": ["id", "seats"]
    }

    schema_journey = {
        "type": "object",
        "properties": {
            "id": { "type": "number"},
            "people": { "type": "number"}
        },
        "required": ["id", "seats"]
    }

def check_format(data):
    if data:
        for k,v in data:
            if k not in schema_cars['properties'].keys():
                print("Key '{}' not in schema ... skipping.".format(k))
                continue

            if ((schema_cars['properties'][k]['type']) or (schema_cars['properties'][v]['type'])) != 'number':
                print("Keys '{}' with wrong types ... skipping.")

            else:
                print("Valid JSON body format.")
                return

def valid_type(type_name, obj):
    if type_name == "number":
        return isinstance(obj, int) or isinstance(obj, float)

    if type_name == "int":
        return isinstance(obj, int)

    if type_name == "float":
        return isinstance(obj, float)

    if type_name == "string":
        return isinstance(obj, str)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=port)
